import Fluent
import Vapor

final class Plane: Model, Content {
    static let schema = "planes"
    
    @ID(key: .id)
    var id: UUID?

    @Field(key: "teamID")
    var teamID: String
    
    @Field(key: "registration")
    var registration: String

    @Field(key: "status")
    var status: String
    
    @Field(key: "note")
    var note: String
    
    @Field(key: "updated")
    var updated: Date

    init() { }

    init(teamID: String, registration: String, status: String, note: String) {
        id = UUID(uuidString: registration + teamID)
        self.teamID = teamID
        self.registration = registration
        self.note = note
        self.updated = Date()
        
        // Catch common slack emoji notation and convert it to proper emoji to save space
        // in the status field value
        switch status {
        case ":white_check_mark:":
            self.status = "✅"
        case ":x:":
            self.status = "❌"
        case ":warning:":
            self.status = "⚠️"
        default:
            self.status = status
        }
    }
}
