//
//  SlashCommand.swift
//  
//
//  Created by William Dillon on 1/1/21.
//

import Vapor

//    token=gIkuvaNzQIHg97ATvDxqgjtO
//    &team_id=T0001
//    &team_domain=example
//    &channel_id=C2147483705
//    &channel_name=test
//    &user_id=U2147483697
//    &user_name=Steve
//    &command=/weather
//    &text=94070
//    &response_url=https://hooks.slack.com/commands/1234/5678
//    &trigger_id=13345224609.738474920.8088930838d88f008e0
//    &api_app_id=A123456

final class SlashCommand: Content {
    let token: String
    let teamID: String
    let teamDomain: String
    let channelID: String
    let channelName: String
    let userID: String
    let userName: String
    let command: String
    let text: String
    let responseURL: String
    let triggerID: String
    let apiAppID: String
    
    internal enum CodingKeys: String, CodingKey {
        case token
        case teamID         = "team_id"
        case teamDomain     = "team_domain"
        case channelID      = "channel_id"
        case channelName    = "channel_name"
        case userID         = "user_id"
        case userName       = "user_name"
        case command
        case text
        case responseURL    = "response_url"
        case triggerID      = "trigger_id"
        case apiAppID       = "api_app_id"
    }
}
