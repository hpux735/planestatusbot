import Fluent
import FluentSQLiteDriver
import Vapor

// configures your application
public func configure(_ app: Application) throws {
    let fileMiddleware = FileMiddleware(
        publicDirectory: app.directory.publicDirectory
    )
    
    app.middleware.use(fileMiddleware)
    
    app.databases.use(.sqlite(.file("db.sqlite")), as: .sqlite)

    app.migrations.add(CreatePlane())

    try routes(app)
}
