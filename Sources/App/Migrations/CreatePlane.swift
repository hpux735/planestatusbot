import Fluent

struct CreatePlane: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        return database.schema("planes")
            .id()
            .field("teamID", .string, .required)
            .field("registration", .string, .required)
            .field("status", .string, .required)
            .field("note", .string)
            .field("updated", .date, .required)
            .create()
    }

    func revert(on database: Database) -> EventLoopFuture<Void> {
        return database.schema("planes").delete()
    }
}
