//
//  File.swift
//  
//
//  Created by William Dillon on 1/3/21.
//

import Foundation
import Vapor

struct TopicController {
    enum TopicError: Error {
        case MissingOauthToken
        case invalidJSON
    }
    
    struct TopicRequest: Encodable {
        let token: String
        let channel: String
        let topic: String
    }
    
    let oauthToken: String
    
    init(tokenFile: String? = nil) {
        let file: String
        if tokenFile == nil { file = "/etc/planeStatusBot/OauthToken.txt" }
        else                { file = tokenFile! }
        
        let url = URL(fileURLWithPath: file)
        do {
            oauthToken = try String(contentsOf: url, encoding: .utf8).trimmingCharacters(in: .newlines)
        } catch {
            oauthToken = ""
        }
    }
    
    func setTopic(req: Request, channel: String, value: String) -> EventLoopFuture<ClientResponse> {
        guard !oauthToken.isEmpty else {
            return req.eventLoop.makeFailedFuture(TopicError.MissingOauthToken)
        }
        
        let topicReq  = TopicRequest(token: oauthToken, channel: channel, topic: value)
        
        do {
            let topicJSON = try JSONEncoder.init().encode(topicReq)
            let headers = HTTPHeaders([
                ("Content-Type", "application/json"),
                ("charset", "utf-8"),
                ("Authorization", "Bearer \(oauthToken)")
            ])
            print(headers)
            print(String(bytes: topicJSON, encoding: .utf8)!)
            return req.application.client.post(
                URI(string: "https://slack.com/api/conversations.setTopic"),
                headers: headers) { (request: inout ClientRequest) in
                request.body = ByteBuffer(data: topicJSON)
            }
        } catch {
            return req.eventLoop.makeFailedFuture(TopicError.invalidJSON)
        }
    }
}
