import Fluent
import Vapor
import Slackmoji

struct PlaneController: RouteCollection {
    let slackmoji = Slackmoji();
    let token: String?
    let topicController: TopicController

    init(_ app: Application) {
        let tokenFile = app.directory.workingDirectory + "Token.txt"
        token = try? String(contentsOf: URL(fileURLWithPath: tokenFile)).trimmingCharacters(in: .newlines)
        
        topicController = TopicController(tokenFile: app.directory.workingDirectory + "OauthToken.txt")
    }

    internal init() {
        token = nil
        topicController = TopicController(tokenFile: nil)
    }
    
    func checkRegistration(_ input: String) -> String? {
        guard input.count >= 3 else { return nil }

        return String(input.suffix(3)).uppercased()
    }

    func boot(routes: RoutesBuilder) throws {
        let planes = routes.grouped("planes")
        planes.get(use: index)
        planes.post(use: create)
        planes.group(":planeID") { plane in
            plane.delete(use: delete)
        }
    }

    func index(req: Request) throws -> EventLoopFuture<[Plane]> {
        return Plane.query(on: req.db).all()
    }

    func create(req: Request) throws -> EventLoopFuture<Plane> {
        let plane = try req.content.decode(Plane.self)
        let future = plane.save(on: req.db).map {
            plane
        }
        return future
    }

    // Update causes the provided aircraft update data to be saved in the db, and the status updated.
    func update(req: Request, cmd: SlashCommand, reg: String, status statusIn: String, note: String) -> EventLoopFuture<String> {
        let status: String
        switch statusIn {
        case ":white_check_mark:":
            status = "✅"
        case ":x:":
            status = "❌"
        case ":warning:":
            status = "⚠️"
        default:
            status = statusIn.trimmingCharacters(in: CharacterSet(charactersIn: Unicode.Scalar(UInt16(0xFE0F))!...Unicode.Scalar(UInt16(0xFE0F))!))
        }
        
        return Plane.query(on: req.db)
            .filter(\.$teamID == cmd.teamID)
            .filter(\.$registration == reg)
            .all()
            .flatMap { (planes) -> EventLoopFuture<String> in
                if planes.count == 1 {
                    planes[0].status = status
                    planes[0].note = note
                    planes[0].updated = Date()
                    return planes[0].update(on: req.db).flatMap {
                        query(req: req, cmd: cmd)
                    }
                } else if planes.count == 0 {
                    let plane = Plane.init(teamID: cmd.teamID, registration: reg, status: status, note: note)
                    return plane.save(on: req.db).flatMap {
                        return query(req: req, cmd: cmd)
                    }
                } else {
                    return req
                        .eventLoop
                        .makeSucceededFuture("Tailnumber collision; ATC says \"Unable\""
                        )
                }
        }
    }
    

    func delete(req: Request) throws -> EventLoopFuture<HTTPStatus> {
        return Plane.find(req.parameters.get("planeID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { $0.delete(on: req.db) }
            .transform(to: .ok)
    }

    func query(req: Request, cmd: SlashCommand, quiet: Bool = false) -> EventLoopFuture<String> {
        return Plane.query(on: req.db)
            .filter(\.$teamID == cmd.teamID)
            .sort(\.$updated, .descending)
            .all()
            .map { (planes) -> String in
                var working = 0
                for plane in planes {
                    let status = plane
                        .status
                        .trimmingCharacters(in: CharacterSet(charactersIn: Unicode.Scalar(UInt16(0xFE0F))!...Unicode.Scalar(UInt16(0xFE0F))!))
                    switch status {
                    case ":white_check_mark:", "✅":
                        working += 1
                    default:
                            break
                    }
//                    print("\(plane.status) (\(plane.status.unicodeScalars.map({$0.value}).map({String($0, radix: 16, uppercase: true) }))): \(working)")
                }

                let percent = Int(Double(working)/Double(planes.count) * 100)
                let header  = "\(working)/\(planes.count) (\(percent)%) of planes are currently ✅\n"
                
                if !quiet {
                    topicController.setTopic(
                        req: req,
                        channel: cmd.channelID,
                        value: planes.reduce(header) { (retval, plane) -> String in
                            let status: String
                            switch plane.status {
                            case ":white_check_mark:":
                                status = "✅"
                            case ":x:":
                                status = "❌"
                            case ":warning:":
                                status = "⚠️"
                            default:
                                status = plane.status
                            }

                            return retval + "\n" + "\(status) \(plane.registration)"
                        }
                    ).whenComplete { result in
                        switch result {
                        case .success(let string):
                            req.logger.debug("Succeeded in sending response to status update\n\(string)")
                        case .failure(let error):
                            req.logger.error("Error sending response to status update: \(dump(error))")
                        }
                    }
                }
                
                return planes.reduce(header) { (retval, plane) -> String in
                    let status: String
                    switch plane.status {
                    case ":white_check_mark:":
                        status = "✅"
                    case ":x:":
                        status = "❌"
                    case ":warning:":
                        status = "⚠️"
                    default:
                        status = plane.status
                    }
                    
                    if plane.note.isEmpty {
                        return retval + "\n" + "\(status) \(plane.registration)"
                    } else {
                        return retval + "\n" + "\(status) \(plane.registration) (\(plane.note))"
                    }
                }
        }
    }
    
    func query(req: Request, cmd: SlashCommand, reg: String) -> EventLoopFuture<String> {
        return Plane.query(on: req.db)
            .filter(\.$teamID == cmd.teamID)
            .filter(\.$registration == reg)
            .all()
            .flatMap { (planes) -> EventLoopFuture<String> in
                if planes.count == 1 {
                    let plane = planes[0]
                    
                    let retval: String
                    if plane.note.isEmpty {
                        retval = "\(plane.status) \(plane.registration)"
                    } else {
                        retval = "\(plane.status) \(plane.registration) (\(plane.note))"
                    }

                    return req.eventLoop.makeSucceededFuture(retval)

                } else {
                    return req
                        .eventLoop
                        .makeSucceededFuture(
                            "Unable to find unique registration with provided value: \(reg)"
                        )
                }
            }
    }
    
    internal func processEmoji(_ input: String) -> String {
        // Pre-process every command from slack to look for emoji.  The theory here is
        // that there must be at least a pair of colons for there to be an emoji.  So,
        // if we take the components of the string seperated by a colon, then we can
        // run each of them through the Slackmoji system.  For any of them that return
        // an emoji, we can replace the contents of that component with the emoji.
        //
        // Examples to work through the thought experiemnts, none of these shouldn't have emoji:
        // :some text with a leading colon
        // some text with a trailing colon
        // some :text: that's a single word in colons that's not an emoji
        // some :text that: has two words in colons, that aren't emoji
        // Some :text :with :nested: colons: like this:
        //
        // Examples to work through that should have emoji replacements:
        // There should be a :heart: in this one.
        // There :should also: be an :x: X: emoji in this one.

        // For each component, try to convert it to emoji.  If it succeeds, return
        // the emoji and a flag that says it was converted.  Otherwise, return the
        // original string and a flag saying it wasn't converted.
        let components = input.components(separatedBy: ":").map {
            component -> (String, Bool) in

            let emoji = slackmoji.shortcodeToEmoji(component.lowercased())

            if emoji.isEmpty { return (component, false) }

            return (emoji.first ?? "", true)
        }

        // Now we have to re-build the string inserting colons before and after
        // each component.  If we do this naively, we'll have two colons on each
        // middle colon, or we'll have a colon after an emoji.  We essentially have
        // to be stateful, and suppress the colon after an emoji as well as before,
        // and non-emoji colons have to include a colon prior to the component, except
        // the first one.
        var output = ""
        for i in 0 ..< components.count {
            // Attempt to look-ahead.  If we we can look ahead one component, and that
            // component is an emoji, we're not going to insert a colon.
            let nextEmoji: Bool
            if (i + 1) < components.count { nextEmoji = components[i+1].1 }
            else { nextEmoji = false }

            output += components[i].0

            // This or the next component isn't an emoji, and not the last component, add a trailing `:`
            if components[i].1 == false && i < (components.count - 1) && !nextEmoji {
                output += ":"
            }
        }

        return output
    }

    func command(req: Request, cmd: SlashCommand) throws -> EventLoopFuture<String> {
        let components = processEmoji(cmd.text).components(separatedBy: .whitespacesAndNewlines)

        switch components.count {

        // No arguments updates the status with the current contents of the database
        case 0:
            return query(req: req, cmd: cmd, quiet: true)

        // One argument replies with the status of that one plane
        case 1:
            // If we have an empty text value, it will still appear as a component, but it'll be empty.
            if components[0].isEmpty {
                return query(req: req, cmd: cmd, quiet: true)
            }

            if let reg = checkRegistration(components[0]) {
                return query(req: req, cmd: cmd, reg: reg)
            } else {
                return req
                    .eventLoop
                    .makeSucceededFuture(
                        "Invalid registration provided; it must be at least 3 characters"
                    )
            }

        // Two arguments sets the status of the registration to only the emoji (and clears the note)
        case 2:
            if let reg = checkRegistration(components[0]) {
                return update(
                    req: req,
                    cmd: cmd,
                    reg: reg,
                    status: components[1], note: "")
            } else {
                return req
                    .eventLoop
                    .makeSucceededFuture(
                        "Invalid registration provided; it must be at least 3 characters"
                    )
            }

        // Three or more arguments sets the status and the note
        default:
            if let reg = checkRegistration(components[0]) {
                return update(
                    req: req,
                    cmd: cmd,
                    reg: reg,
                    status: components[1],
                    note: components.suffix(from: 2).joined(separator: " "))
            } else {
                return req
                    .eventLoop
                    .makeSucceededFuture(
                        "Invalid registration provided; it must be at least 3 characters"
                    )
            }
        }
    }
}
