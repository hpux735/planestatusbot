import Fluent
import Vapor

func statusToResponse(_ code: HTTPStatus) -> Response {
    return Response(status: HTTPResponseStatus(statusCode: Int(code.code)),
                    version: HTTPVersion(major: 1, minor: 1),
                    headersNoUpdate: HTTPHeaders(),
                    body: Response.Body.init(string: code.reasonPhrase))
}

func routes(_ app: Application) throws {
    let controller = PlaneController(app)
    try app.register(collection: controller)

    app.post("planeStatus") { (req: Request) -> EventLoopFuture<Response> in
        do {
            let command = try req.content.decode(SlashCommand.self)
            
            if let bodyString = req.body.string {
                req.logger.trace("Got slash command:\n\(bodyString)")
            }
            
            req.logger.debug("content:\n\(dump(command))")
            
            // If we have a token file verify the token supplied by Slack
            if let token = controller.token {
                guard command.token == token else {
                    req.logger.info("Command sent with invalid token: \(command.token)")
                    
                    return req.eventLoop.makeSucceededFuture(
                        statusToResponse(HTTPStatus(statusCode: 500, reasonPhrase: "Server sent command with invalid token"))
                    )
                }
            }
            
            // Send the command to the controller
            return try controller.command(req: req, cmd: command).map { string in
                Response(status: .ok, version: HTTPVersion(major: 1, minor: 1), headers: HTTPHeaders(), body: Response.Body.init(string: string))
            }
    
        } catch {
            return req.eventLoop.makeSucceededFuture(
                statusToResponse(HTTPStatus(statusCode: 500, reasonPhrase: "Invalid command body: \(dump(error))"))
            )
        }
    }

}
