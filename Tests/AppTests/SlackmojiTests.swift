//
//  SlackmojiTests.swift
//  
//
//  Created by William Dillon on 11/13/21.
//

import XCTest
@testable import App

class ElecFurnaceFunctionalTests: XCTestCase {

    static let allTests = [
        ("testSlackmoji",    testSlackmoji),
    ]

    func testSlackmoji() {
        let planeController = PlaneController()

        let testSet: Array<(String, String)> = [
            (":some text with a leading colon", ":some text with a leading colon"),
            ("some text with a trailing colon:", "some text with a trailing colon:"),
            ("some :text: that's a single word in colons that's not an emoji", "some :text: that's a single word in colons that's not an emoji"),
            ("some :text that: has two words in colons, that aren't emoji", "some :text that: has two words in colons, that aren't emoji"),
            ("Some :text :with :nested: colons: like this:","Some :text :with :nested: colons: like this:"),
            ("There should be a :HEART: in this one.", "There should be a ❤️ in this one."),
            ("There :should also: be an :x: X: emoji in this one.", "There :should also: be an ❌️ X: emoji in this one.")
        ]

        for test in testSet {
            XCTAssertEqual(planeController.processEmoji(test.0), test.1)
        }
    }
}
